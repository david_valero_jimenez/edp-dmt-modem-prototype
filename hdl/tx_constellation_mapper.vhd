--------------------------------------------------------------------------------
--
-- Non-generalistic implementation of constellation mapping (TX case)
--
--      Number of phases:       4 (PI/4, 3*PI/4, -3*PI/4, -PI/4)
--      Number of amplitudes:   2 (1 and 2)
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;
use ieee.math_real.MATH_PI_OVER_4;
use ieee.math_real.cos;
use ieee.math_real.sin;

library work;
use work.types.all;
use work.modem_config.all;



entity S2_tx_constellation_mapper is

  port (
    clk : in std_logic;                                     -- Clock
    rst : in std_logic;                                     -- Reset
    we : in std_logic;                                      -- Write Enable
    frame : in std_logic_vector (FRAME_WIDTH-1 downto 0);   -- Frame input
    xr, xi : out  fixed_vector(0 to N-1);                   -- Spectrum complex output
    ready : out std_logic                                   -- Data ready signal
  );

end S2_tx_constellation_mapper;



architecture rtl of S2_tx_constellation_mapper is

    type states is ( zeroMatrix, init, mapping );
    signal state, next_state : states;

    type real_vector is array (natural range <>) of real;
    constant amps : real_vector(N_AMPS-1 downto 0) := ( real(2), real(1) );
    constant phases : real_vector(N_PHASES-1 downto 0) := ( -MATH_PI_OVER_4, -real(3)*MATH_PI_OVER_4, real(3)*MATH_PI_OVER_4, MATH_PI_OVER_4 );
    constant N_CODES : integer := N_AMPS * N_PHASES;
    constant cr : fixed_vector(0 to N_CODES-1) := (
        to_fixed(  amps(0) * cos(phases(0))  ),
        to_fixed(  amps(0) * cos(phases(1))  ),
        to_fixed(  amps(0) * cos(phases(2))  ),
        to_fixed(  amps(0) * cos(phases(3))  ),
        to_fixed(  amps(1) * cos(phases(0))  ),
        to_fixed(  amps(1) * cos(phases(1))  ),
        to_fixed(  amps(1) * cos(phases(2))  ),
        to_fixed(  amps(1) * cos(phases(3))  )
    );
    constant ci : fixed_vector(0 to N_CODES-1) := (
        to_fixed(  amps(0) * sin(phases(0))  ),
        to_fixed(  amps(0) * sin(phases(1))  ),
        to_fixed(  amps(0) * sin(phases(2))  ),
        to_fixed(  amps(0) * sin(phases(3))  ),
        to_fixed(  amps(1) * sin(phases(0))  ),
        to_fixed(  amps(1) * sin(phases(1))  ),
        to_fixed(  amps(1) * sin(phases(2))  ),
        to_fixed(  amps(1) * sin(phases(3))  )
    );
    constant ci2 : fixed_vector(0 to N_CODES-1) := (
        to_fixed(  -amps(0) * sin(phases(0))  ),
        to_fixed(  -amps(0) * sin(phases(1))  ),
        to_fixed(  -amps(0) * sin(phases(2))  ),
        to_fixed(  -amps(0) * sin(phases(3))  ),
        to_fixed(  -amps(1) * sin(phases(0))  ),
        to_fixed(  -amps(1) * sin(phases(1))  ),
        to_fixed(  -amps(1) * sin(phases(2))  ),
        to_fixed(  -amps(1) * sin(phases(3))  )
    );

    constant ZERO : fixed := to_fixed(0);

begin

    SEC: process(clk,rst)
    begin

        if rst = '1' then
            
            state <= zeroMatrix;

        elsif rising_edge(clk) then

            --if we = '1' then
                --state <= mapping;
            --else
                state <= next_state;
            --end if;

        end if;

    end process;


    COMB : process(state,we,frame)
        variable lr, li : fixed_vector(0 to N-1);
    begin
        ready <= '1';
        next_state <= init;

        case state is

            when zeroMatrix =>
                for i in 0 to N-1 loop
                    lr(i) := ZERO;
                    li(i) := ZERO;
                end loop;

            when init =>
                if we = '1' then
                    ready <= '0';
                    next_state <= mapping;
                end if;

            when mapping =>
                for i in 1 to N_CARRIERS loop
                    lr(i) := cr(  to_integer(unsigned( frame(3*(i-1)+2 downto 3*(i-1)) ))  );
                    li(i) := ci(  to_integer(unsigned( frame(3*(i-1)+2 downto 3*(i-1)) ))  );
                    lr(N-i) := cr(  to_integer(unsigned( frame(3*(i-1)+2 downto 3*(i-1)) ))  );
                    li(N-i) := ci2(  to_integer(unsigned( frame(3*(i-1)+2 downto 3*(i-1)) ))  );
                end loop;

        end case;

        for i in 0 to N-1 loop
            xr(i) <= lr(i);
            xi(i) <= li(i);
        end loop;

    end process;

end rtl;
