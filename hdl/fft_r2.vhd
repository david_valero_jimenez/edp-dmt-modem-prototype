library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;
use ieee.math_real.sqrt;

library work;
use work.types.all;
use work.modem_config.all;
use work.bit_reverse_8.all;


entity fft_r2 is
  
  generic (
    N : integer;                            -- FFT size
    inv : in std_logic                      -- Inverse transform if high level
  );

  port (
    clk: in std_logic;                      -- Clock
    rst : in std_logic;                     -- Asynchronous reset
    frame_ready : in std_logic;             -- Input matrix updated
    xr, xi : in  fixed_vector (0 to N-1);   -- Complex input vector
    yr, yi : out fixed_vector (0 to N-1);   -- Complex output vector
    ready : out std_logic                   -- Output data ready
  );

end fft_r2;



architecture rtl of fft_r2 is

    component fft_ram
        port (
            clk     : in    std_logic;
            rst     : in    std_logic;
            addr1   : in    integer;
            din1_r  : in fixed;
            din1_i  : in fixed;
            dout1_r  : out fixed;
            dout1_i  : out fixed;
            addr2   : in    integer;
            din2_r  : in fixed;
            din2_i  : in fixed;
            dout2_r  : out fixed;
            dout2_i  : out fixed;
            we      : in    std_logic
        );
    end component;

    type states is ( Init, BRFrame, WaitBR, InitCompute, Compute, Compute2, Assign, Output, WaitOutput );
    signal state, next_state : states;

    signal addr1, addr2 : integer;
    signal din1_r, din1_i, din2_r, din2_i : fixed;
    signal dout1_r, dout1_i, dout2_r, dout2_i : fixed;
    signal write_ram : std_logic;

    signal mr, mi : fixed;

    constant ZERO : fixed := to_fixed(0);

begin

    ram1: fft_ram
        port map (
            clk => clk,
            rst => rst,
            addr1 => addr1,
            din1_r => din1_r,
            din1_i => din1_i,
            dout1_r => dout1_r,
            dout1_i => dout1_i,
            addr2 => addr2,
            din2_r => din2_r,
            din2_i => din2_i,
            dout2_r => dout2_r,
            dout2_i => dout2_i,
            we => write_ram
        );


    SEC: process(clk,rst)
    begin
        if rst = '1' then
            state <= Init;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;


    COMB: process (state,frame_ready)

        variable i : integer := 0;                       -- Stage number
        variable b : integer := 0;                       -- Butterfly number
        variable k : integer := 0;                  -- Twiddle number
        variable k1 : integer := 0;                 -- First element number
        variable k2 : integer := 1;                 -- Second element number

        variable out_r, out_i : fixed_vector(0 to N-1);

    begin

        write_ram <= '0';
        next_state <= Init;

        case state is


            when Init =>
                ready <= '0';
                if frame_ready = '1' then
                    k1 := 0;
                    k2 := 1;
                    next_state <= BRFrame;
                else
                    next_state <= Init;
                end if;


            when BRFrame =>
                ready <= '0';
                write_ram <= '1';
                din1_r <= xr(reverseNBitIndex(3,addr1));
                din1_i <= xi(reverseNBitIndex(3,addr1));
                din2_r <= xr(reverseNBitIndex(3,addr2));
                din2_i <= xi(reverseNBitIndex(3,addr2));
                if k1 = N/2+2 then
                    next_state <= InitCompute;
                else
                    next_state <= WaitBR;
                end if;


            when WaitBR =>
                ready <= '0';
                k1 := k1 + 2;
                k2 := k2 + 2;
                next_state <= BRFrame;


            when InitCompute =>
                i := 0;
                b := 0;
                k := 0;
                k1 := 0;
                k2 := Nb(i)/2;
                next_state <= Compute;


            when Compute =>
                ready <= '0';
                if inv = '1' then 
                -- add and sub operators are inverted to avoid WN additive inverse LUT
                    mr <= fadd(  fmul(wnr(k),dout2_r), fmul(wni(k),dout2_i)  );
                    mi <= fsub(  fmul(wnr(k),dout2_i), fmul(wni(k),dout2_r)  );
                else
                    mr <= fsub(  fmul(wnr(k),dout2_r), fmul(wni(k),dout2_i)  );
                    mi <= fadd(  fmul(wnr(k),dout2_i), fmul(wni(k),dout2_r)  );
                end if;
                next_state <= Compute2;

            when Compute2 =>

                din1_r <= fadd( dout1_r, mr );
                din1_i <= fadd( dout1_i, mi );
                din2_r <= fsub( dout1_r, mr );
                din2_i <= fsub( dout1_i, mi );

                write_ram <= '1';

                next_state <= Assign;


            when Assign =>
                ready <= '0';
                -- Variables update
                k := k + B_number(i);
                k1 := k1 + 1;
                k2 := k2 + 1;
                if k1 = b*Nb(i)+Nb(i)/2 then
                    b := b + 1;
                    k := 0;
                    k1 := b*Nb(i);
                end if;
                if b = B_number(i) then
                    i := i + 1;
                    b := 0;
                    k1 := 0;
                end if;
                if i = S then
                    i := 0;
                    next_state <= Output;
                    k1 := 0;
                    k2 := 1;
                else
                    next_state <= Compute;
                end if;
                k2 := k1 + Nb(i)/2;


            when Output =>
                ready <= '0';
                write_ram <= '0';

                if inv = '1' then
                    out_r(addr1) := fdiv( dout1_r, N );
                    out_i(addr1) := fdiv( dout1_i, N );
                    out_r(addr2) := fdiv( dout2_r, N );
                    out_i(addr2) := fdiv( dout2_i, N );
                else
                    out_r(addr1) := dout1_r;
                    out_i(addr1) := dout1_i;
                    out_r(addr2) := dout2_r;
                    out_i(addr2) := dout2_i;
                end if;

                if k1 = N/2+2 then
                    ready <= '1';
                    next_state <= Init;
                else
                    next_state <= WaitOutput;
                end if;


            when WaitOutput =>
                ready <= '0';
                k1 := k1 + 2;
                k2 := k2 + 2;
                next_state <= Output;

        end case;

        yr <= out_r;
        yi <= out_i;
        addr1 <= k1;
        addr2 <= k2;

    end process;

end rtl;
