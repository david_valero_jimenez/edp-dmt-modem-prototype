library ieee, work;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.types.all;
use work.modem_config.all;



entity fft_ram is

    port (
        clk     : in    std_logic;
        rst     : in std_logic;
        addr1   : in    integer;
        din1_r  : in fixed;
        din1_i  : in fixed;
        dout1_r  : out fixed;
        dout1_i  : out fixed;
        addr2   : in    integer;
        din2_r  : in fixed;
        din2_i  : in fixed;
        dout2_r  : out fixed;
        dout2_i  : out fixed;
        we      : in    std_logic
    );

end entity fft_ram;



architecture rtl of fft_ram is

    signal mem_r : fixed_vector(0 to N-1);
    signal mem_i : fixed_vector(0 to N-1);
    signal sout1_r, sout1_i : fixed;
    signal sout2_r, sout2_i : fixed;

begin  -- rtl

    MEM_WR:
    process (clk,rst) begin
        if rst = '1' then
            for i in 0 to N-1 loop
                mem_r(i) <= to_fixed(0);
                mem_i(i) <= to_fixed(0);
            end loop;
        elsif rising_edge(clk) then
            if we = '1' then
                mem_r(addr1) <= din1_r;
                mem_i(addr1) <= din1_i;
                mem_r(addr2) <= din2_r;
                mem_i(addr2) <= din2_i;
            end if;
        end if;
    end process MEM_WR;

    MEM_RD:
    process (clk,rst) begin
        if rst = '1' then
            null;
        elsif rising_edge(clk) then
            if we = '0' then
                dout1_r <= mem_r(addr1);
                dout1_i <= mem_i(addr1);
                dout2_r <= mem_r(addr2);
                dout2_i <= mem_i(addr2);
                --sout1_r <= mem_r(addr1);
                --sout1_i <= mem_i(addr1);
                --sout2_r <= mem_r(addr2);
                --sout2_i <= mem_i(addr2);
            end if;
        end if;
    end process MEM_RD;
--
    --dout1_r <= sout1_r when (we='1') else (others=>'Z');
    --dout1_i <= sout1_i when (we='1') else (others=>'Z');
    --dout2_r <= sout2_r when (we='1') else (others=>'Z');
    --dout2_i <= sout2_i when (we='1') else (others=>'Z');

end architecture rtl;
