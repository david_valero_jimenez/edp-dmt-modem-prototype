library ieee, work;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.types.all;
use work.modem_config.all;



package bit_reverse_8 is

    function reverseNBitIndex ( constant NB : in integer; i : in integer ) return integer;

end package bit_reverse_8;



package body bit_reverse_8 is

    function bitReverse_std_logic_vector ( x : in std_logic_vector ) return std_logic_vector is
        variable result : std_logic_vector(x'RANGE);
        alias r : std_logic_vector(x'REVERSE_RANGE) is x;
    begin
        for i in r'RANGE loop
            result(i) := r(i);
        end loop;
        return result;
    end function bitReverse_std_logic_vector;


    function reverseNBitIndex ( constant NB : in integer; i : in integer ) return integer is
    begin
        return to_integer(unsigned(  bitReverse_std_logic_vector(std_logic_vector(to_unsigned(i,NB))) ));
    end function reverseNBitIndex;

end package body;
