--------------------------------------------------------------------------------
-- University: UCLM, Castilla - La Mancha
-- Project: Desing, simulation and prototyping of DMT modem (End-of-degree)
--
-- File: modem.vhd
--
-- Description:
--
-- Top-level entity of DMT modem prototype
--
-- Targeted device: <Family::IGLOO+> <Die::AGLP125V2> <Package::289 CS>
-- Author: David Valero Jimenez
--
--------------------------------------------------------------------------------

library IEEE, work;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;

use work.modem_config.all;

entity modem is
  port (
    clk : in std_logic;     -- Clock signal
    rst : in std_logic;     -- Reset signal
    frame : in std_logic_vector(FRAME_WIDTH-1 downto 0);
    ssel : out std_logic;   -- SPI Select-Slave signal
    sclk : out std_logic;   -- Serial clock signal
    dout : out std_logic    -- Digital output
  );
end modem;


architecture rtl of modem is

    component modulator
        port (
            clk : in std_logic;
            rst : in std_logic;
            we : in std_logic;
            frame : in  std_logic_vector(FRAME_WIDTH-1 downto 0);
            ready : out std_logic;
            ssel : out std_logic;   -- SPI Select-Slave signal
            sclk : out std_logic;   -- Serial clock signal
            dout : out std_logic    -- Digital output
        );
    end component;


    type states is ( WaitReady, PushFrame );
    signal state, next_state : states;

    signal mod_we, mod_ready : std_logic;
    --signal frame : std_logic_vector(FRAME_WIDTH-1 downto 0);

begin

    modulator1 : modulator
        port map (
            clk => clk,
            rst => rst,
            we => mod_we,
            frame => frame,
            ready => mod_ready,
            ssel => ssel,
            sclk => sclk,
            dout => dout
        );


    SEC: process(clk,rst)
    begin

        if rst = '1' then

            -- State initialization
            state <= WaitReady;

        elsif rising_edge(clk) then

            -- State update
            state <= next_state;

        end if;

    end process;


    COMB: process(state,mod_ready)
    begin

        mod_we <= '0';
        next_state <= WaitReady;

        case state is

            --when Init =>
                --mod_we <= '0';
                --next_state <= WaitReady;

            when WaitReady =>
                mod_we <= '0';
                if mod_ready = '1' then
                    next_state <= PushFrame;
                else
                    next_state <= WaitReady;
                end if;

            when PushFrame =>
                --frame <= "010110";
                mod_we <= '1';
                next_state <= WaitReady;

        end case;

    end process;


end rtl;
