--------------------------------------------------------------------------------
-- University: UCLM, Castilla - La Mancha
-- Project: Desing, simulation and prototyping of DMT modem (End-of-degree)
--
-- File: modulator.vhd
--
-- Description: 
--
-- Top-level entity of DMT modem prototype
--
-- Targeted device: <Family::IGLOO+> <Die::AGLP125V2> <Package::289 CS>
-- Author: David Valero Jimenez
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;

library work;
use work.types.all;
use work.modem_config.all;



entity modulator is
  port (
    clk : in std_logic;                                     -- Clock signal
    rst : in std_logic;                                     -- Asynchronous reset
    we : in std_logic;                                      -- Write enable
    frame : in std_logic_vector (FRAME_WIDTH-1 downto 0);   -- Frame parallel input
    ready : out std_logic;                                  -- Ready for frame
    ssel : out std_logic;                                   -- SPI Select-Slave signal
    sclk : out std_logic;                                   -- SPI Serial clock signal
    dout : out std_logic                                    -- SPI Digital output
  );
end modulator;



architecture rtl of modulator is

    component S2_tx_constellation_mapper
        port (
            clk : in std_logic;
            rst : in std_logic;
            we : in std_logic;
            frame : in std_logic_vector (FRAME_WIDTH-1 downto 0);
            xr, xi : out  fixed_vector;
            ready : out std_logic
        );
    end component;

    component fft_r2
        generic (
            N : integer;
            inv : std_logic
        );
        port (
            clk : in std_logic;
            rst : in std_logic;
            xr, xi : in  fixed_vector;
            yr : out fixed_vector(0 to N-1);
            yi : out fixed_vector(0 to N-1);
            frame_ready : in std_logic;
            ready : out std_logic
        );
    end component;


    signal xr, xi : fixed_vector (0 to N-1);
    signal yr, yi : fixed_vector (0 to N-1);
    signal cmapper_we, data_ready, frame_ready, fft_ready, tds_ready, full_cycle : std_logic;

    type states is ( Init, InitCycle, WaitFrame, WaitTX, IFFTComputation, WaitTDSignal );
    signal state, next_state : states;

    type spi_states is ( InitSPI, WaitTDS, SPIWrite, SPINextWrite, SPIWait );
    signal spi_state, next_spi_state : spi_states;

    signal index, bindex : integer;
    signal index_n, bindex_n : integer;

    signal spi_we : std_logic;

    signal frame_complete : std_logic;
    signal frame_complete_n : std_logic;

    signal dout_n : std_logic;
    signal ssel_n : std_logic;
    signal data : std_logic_vector(0 to 31);
    signal data_n : std_logic_vector(0 to 31);

    signal spi_data : std_logic_vector(0 to 31);
    signal spi_req : std_logic;

    signal miso : std_logic;

    signal ssel_reg, sclk_reg, dout_reg, sclk_ena : std_logic;

begin

    txcm1 : S2_tx_constellation_mapper
        port map (
            clk => clk,
            rst => rst,
            we => cmapper_we,
            frame => frame,
            xr => xr, xi => xi,
            ready => data_ready
        );


    fft1 : fft_r2
        generic map (
            N => N,
            inv => '1'
        )
        port map (
            clk => clk,
            rst => rst,
            xr => xr, xi => xi,
            yr => yr, yi => yi,
            frame_ready => frame_ready,
            ready => fft_ready
        );


    SEC: process(clk,rst)
    begin

        if rst = '1' then

            -- State initialization
            state <= Init;

        elsif rising_edge(clk) then

            -- State update
            state <= next_state;

        end if;

    end process;


    COMB: process(state,we,data_ready,fft_ready,frame_complete)
        variable tx_done : std_logic;
    begin

        cmapper_we <= '0';
        frame_ready <= '0';
        tds_ready <= '0';
        ready <= '0';
        tx_done := frame_complete;
        next_state <= Init;

        case state is

            when Init =>
                ready <= '0';
                --tx_done := '1';
                next_state <= InitCycle;

            when InitCycle =>
                ready <= '1';
                next_state <= WaitFrame;

            when WaitFrame =>
                ready <= '1';
                next_state <= WaitFrame;
                if we = '1' then
                    ready <= '0';
                    next_state <= WaitTX;
                end if;

            when WaitTX =>
                ready <= '0';
                next_state <= WaitTX;
                if tx_done = '1' then
                    tx_done := '0';
                    cmapper_we <= '1';
                    next_state <= IFFTComputation;
                end if;

            when IFFTComputation =>
                ready <= '0';
                next_state <= IFFTComputation;
                if data_ready = '1' then
                    frame_ready <= '1';
                    next_state <= WaitTDSignal;
                end if;

            when WaitTDSignal =>
                ready <= '0';
                next_state <= WaitTDSignal;
                if fft_ready = '1' then
                    tds_ready <= '1';
                    next_state <= InitCycle;
                end if;

        end case;

    end process;


    SEC_SPI: process(clk,rst)
    begin

        --sclk <= clk and not ssel_n; -- SPI clock assignment
        sclk <= clk; -- SPI clock assignment

        if rst = '1' then

            -- SPI signals reset
            ssel <= '1';
            index <= 0;
            bindex <= 0;
            data <= "0000" & "0100" & "0000" & "000000000000" & "00000000";
            spi_we <= '0';
            frame_complete <= '0';
            -- SPI state initialization
            spi_state <= InitSPI;

        elsif rising_edge(clk) then

            -- SPI signals update
            ssel <= ssel_n;
            index <= index_n;
            bindex <= bindex_n;
            data <= data_n;
            spi_we <= not ssel_n;
            frame_complete <= frame_complete_n;
            -- SPI state update
            spi_state <= next_spi_state;

        end if;

    end process;


    COMB_SPI: process(spi_state,tds_ready,frame_complete,index,bindex,spi_we)
    begin

        frame_complete_n <= '0';
        data_n <= "0000" & "0100" & "0000" & "000000000000" & "00000000";

        case spi_state is

            when InitSPI =>
                ssel_n <= '0';
                index_n <= 0;
                bindex_n <= 0;
                data_n <= "0000" & "0100" & "0000" & "000000000000" & "00000000";
                next_spi_state <= InitSPI;
                if spi_we = '1' then
                    if bindex = 31 then
                        frame_complete_n <= '1';
                        next_spi_state <= WaitTDS;
                    else
                        bindex_n <= bindex + 1;
                    end if;
                end if;

            when WaitTDS =>
                ssel_n <= '1';
                index_n <= 0;
                bindex_n <= 0;
                data_n <= "0000" & "0100" & "0000" & "000000000000" & "00000000";
                frame_complete_n <= '1';
                next_spi_state <= WaitTDS;
                if tds_ready = '1' then
                    next_spi_state <= SPIWrite;
                end if;

            when SPIWrite =>
                ssel_n <= '0';
                index_n <= index;
                bindex_n <= bindex;
                next_spi_state <= SPIWrite;
                --data := "0000" & "0011" & "0000" & to_slv(fadd(yr(index),to_fixed(31))) & "00000000";
                data_n <= "0000" & "0011" & "0000" & to_slv(yr(index)) & "00000000";
                if frame_complete = '1' then
                    -- Next state (samples serialization)
                    bindex_n <= 0;
                    index_n <= 0;
                elsif spi_we = '1' then
                    if bindex = 31 then
                        ssel_n <= '1';
                        bindex_n <= 0;
                        next_spi_state <= SPIWait;
                    else
                        bindex_n <= bindex + 1;
                    end if;
                end if;

            when SPINextWrite =>
                    data_n <= "0000" & "0011" & "0000" & to_slv(yr(index)) & "00000000";
                ssel_n <= '0';
                index_n <= index;
                bindex_n <= bindex + 1;
                next_spi_state <= SPIWrite;
                

            when SPIWait =>
                ssel_n <= '1';
                data_n <= "0000" & "0011" & "0000" & to_slv(yr(index)) & "00000000";
                index_n <= index;
                bindex_n <= bindex;
                if index = N-1 then
                    index_n <= 0;
                    frame_complete_n <= '1';
                else
                    index_n <= index + 1;
                end if;
                next_spi_state <= SPIWrite;

        end case;

    end process;

    SERIALIZE_COMB: process(bindex,spi_we)
    begin
        if spi_we = '1' then
            dout <= data(bindex);
        else
            dout <= '0';
        end if;
    end process;

end rtl;
