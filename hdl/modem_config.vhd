library IEEE, work;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.MATH_PI;
use ieee.math_real.log2;
use ieee.math_real.ceil;
use ieee.math_real.cos;
use ieee.math_real.sin;

use work.types.all;


package modem_config is

    constant FRAME_WIDTH : integer := 6;                                    -- Number of frame bits

    constant f_s : integer := 2000;                                         -- Sampling frequency (Hz)
    constant T_s : delay_length := 500 us;                                  -- Sampling period (s)
    constant f_0 : integer := 250;                                          -- Sampling frequency (Hz)
    constant T_0 : delay_length := 33333 us;                                -- Sampling period (s)

    constant N_CARRIERS : integer := 2;                                     -- Number of carriers
    constant N_PHASES : integer := 4;                                       -- Number of phases
    constant N_AMPS : integer := 2;                                         -- Number of amplitudes

    constant N : integer := 2**integer( ceil(log2(real(f_s)/real(f_0))) );  -- Size of Discrete Fourier Transform
    constant S : integer := integer(log2(real(N)));                         -- Number of FFT stages

    signal B_number : integer_vector(0 to S-1) := (4,2,1);                -- Number of butterflies
    signal Nb : integer_vector(0 to S-1) := (2,4,8);                      -- Size of butterflies

    -- Twiddle factor
    constant WN : real := real(-2)*MATH_PI/real(N);
    signal wnr : fixed_vector(0 to N/2-1) := (
                                                    to_fixed(cos(real(0))),
                                                    to_fixed(cos(WN)),
                                                    to_fixed(cos(real(2)*WN)),
                                                    to_fixed(cos(real(3)*WN))
                                                );
    signal wni : fixed_vector(0 to N/2-1) := (
                                                    to_fixed(sin(real(0))),
                                                    to_fixed(sin(WN)),
                                                    to_fixed(sin(real(2)*WN)),
                                                    to_fixed(sin(real(3)*WN))
                                                );

end package modem_config;
