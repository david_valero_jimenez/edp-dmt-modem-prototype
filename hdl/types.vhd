--------------------------------------------------------------------------------
-- Types and arithmetics functions for fixed-point real numbers
--   Intended to work in compilation time
--------------------------------------------------------------------------------

library IEEE, work;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;



package types is

    -- Type constants
    constant UB : integer := 5;
    constant LB : integer := -6;

    -- Fixed-point type definition
    subtype fixed is sfixed (UB downto LB);
    type fixed_vector is array (natural range <>) of fixed;

    -- Conversion functions
    function to_fixed ( x : in integer ) return fixed;
    function to_fixed ( x : in real ) return fixed;

    -- Some arithmetics functions
    function fdiv ( x : in fixed; num : in integer ) return fixed;
    function fdiv ( x : in fixed; num : in real ) return fixed;
    function fadd ( x : in fixed; y : in fixed ) return fixed;
    function fsub ( x : in fixed; y : in fixed ) return fixed;
    function fmul ( x : in fixed; y : in fixed ) return fixed;

end package types;



package body types is

    -- Conversion functions

    function to_fixed ( x : in integer ) return fixed is
    begin
        return to_sfixed(x, UB, LB);
    end function to_fixed;


    function to_fixed ( x : in real ) return fixed is
    begin
        return to_sfixed(x, UB, LB);
    end function to_fixed;


    -- Some arithmetic functions

    function fdiv ( x : in fixed; num : in integer ) return fixed is
    begin
        return resize(x/num, x'high, x'low);
    end function fdiv;


    function fdiv ( x : in fixed; num : in real ) return fixed is
    begin
        return resize(x/num, x'high, x'low);
    end function fdiv;


    function fadd ( x : in fixed; y : in fixed ) return fixed is
    begin
        return resize(x+y, x'high, x'low);
    end function fadd;


    function fsub ( x : in fixed; y : in fixed ) return fixed is
    begin
        return resize(x-y, x'high, x'low);
    end function fsub;


    function fmul ( x : in fixed; y : in fixed ) return fixed is
    begin
        return resize(x*y, x'high, x'low);
    end function fmul;

end package body types;
