--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: modem_tb.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::IGLOO+> <Die::AGLP125V2> <Package::289 CS>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.modem_config.all;

entity modem_tb is

end modem_tb;

architecture rtl of modem_tb is

component modem
  port (
    clk : in std_logic;     -- Clock signal
    rst : in std_logic;     -- Reset signal
    frame : in std_logic_vector;
    ssel : out std_logic;   -- SPI Select-Slave signal
    sclk : out std_logic;   -- Serial clock signal
    dout : out std_logic    -- Digital output
  );
end component;


  signal clk : std_logic := '0';
  signal rst : std_logic;
  signal aux_ssel : std_logic;
  signal aux_sclk : std_logic;
  signal aux_dout : std_logic;
  signal frame : std_logic_vector(FRAME_WIDTH-1 downto 0);

begin

    modem1 : modem
    port map (
        clk => clk,
        rst => rst,
        frame => frame,
        ssel => aux_ssel,
        sclk => aux_sclk,
        dout => aux_dout
    );

    process
    begin  -- process
        clk <= not clk;
        wait for 50 ns;
    end process;

    process
    begin
        rst <= '1';
        wait for 2000 ns;
        rst <= '0';
        --wait for 100 ns;
        frame <= "010110";
        wait;
    end process;
    

end rtl;
